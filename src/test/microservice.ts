import { MicroService } from '../microservice'
import * as assert from 'assert';
import * as url from 'url';
import * as mocha from 'mocha';
import * as request from 'supertest';

describe('microservice', () => {
    let service: MicroService;

    before(async () => {
        service = await MicroService.create();
        service.route('/', (request, response) => response.send('Hello World'));
        service.route('/object', (request, response) => response.send({data: Math.random()}));
        service.route('/random', (request, response) => response.send('' + Math.random()));
        service.route('/params', (request, response) => response.send('' + Math.random()),
            {method: 'GET', mandatoryQueryParameters: ['test']});
        service.route('/header', (request, response) =>
            response.header('test', request.query.test).send(),
            {method: 'GET', mandatoryQueryParameters: ['test']});
    });
    beforeEach(() => {
        service.stop();
        service.clearCache();
        service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024);
    });
    afterEach(() => {
        service.stop();
    });

    describe('cache', () => {

        it('records incoming request', async () => {
            const url = '/?q=' + Math.random().toString();
            const res = await request(service.server).get(url);
            const row = await service.db.get('SELECT * FROM cache LIMIT 1');
            assert.equal(row.url, url);
        });

        it('repeats consecutive requests', async () => {
            const path = '/random';
            const res1 = await request(service.server).get(path);
            const row = await service.db.get(`
                SELECT * FROM cache
                WHERE url=(?) AND
                timestamp > datetime('now', '-${service.cacheExpirySeconds} seconds')
                ORDER BY timestamp DESC LIMIT 1`, [path]);
            const res2 = await request(service.server).get(path);
            assert.equal(row.body, res1.text);
            assert.equal(res1.text, res2.text);
        });

        it('expires', async () => {
            const path = '/random';
            const seconds = service.cacheExpirySeconds;
            service.cacheExpirySeconds = 1;
            const res1 = await request(service.server).get(path);
            await new Promise<void>((resolve, _) => setTimeout(resolve, 1000));
            const res2 = await request(service.server).get(path);
            assert.notEqual(res1.text, res2.text);
            service.cacheExpirySeconds = seconds;
        });

        it('stores non-string output', async () => {
            const path = '/object';
            const res1 = await request(service.server).get(path);
            const row = await service.db.get(`
                SELECT * FROM cache
                WHERE url=(?) AND
                timestamp > datetime('now', '-${service.cacheExpirySeconds} seconds')
                ORDER BY timestamp DESC LIMIT 1`, [path]);
            const res2 = await request(service.server).get(path);
            assert.equal(row.body, res1.text);
            assert.equal(res1.text, res2.text);
        });

        it('respects globalCache setting', async () => {
            const path = '/random';
            const res1 = await request(service.server).get(path).set('test', 'foobar');
            const row = await service.db.get(`
                SELECT * FROM cache
                WHERE url=(?) AND
                timestamp > datetime('now', '-${service.cacheExpirySeconds} seconds')
                ORDER BY timestamp DESC LIMIT 1`, [path]);
            const res2 = await request(service.server).get(path).set('test', 'barfoo');
            assert.notEqual(res1.text, res2.text);
        });

        it('replays outbound headers', async () => {
            const val = '' + Math.random();
            const path = '/header?test=' + val;
            const res1 = await request(service.server).get(path);
            const row = await service.db.get(`
                SELECT * FROM cache
                WHERE url=(?) AND
                timestamp > datetime('now', '-${service.cacheExpirySeconds} seconds')
                ORDER BY timestamp DESC LIMIT 1`, [path]);
            const res2 = await request(service.server).get(path);
            const stored = JSON.parse(row.headers_out).test;
            assert.equal(res1.header.test, val);
            assert.equal(res1.header.test, stored);
            assert.equal(res1.header.test, res2.header.test);
        });

    });
    
    describe('route', () => {

        it('created dinamically', done => {
            const path = '/testroute';
            service.route(path, (request, response) => response.send({status: 'OK'}));
            request(service.server).get(path).expect(200, done);
        });

        it('with truthy mandatory parameters', done => {
            request(service.server).get('/params').expect(400, res => {
                request(service.server).get('/params?test=null').expect(200, done);
            });
        });

        it('with falsy mandatory parameters', done => {
            request(service.server).get('/params').expect(400, res => {
                request(service.server).get('/params?test=').expect(200, done);
            });
        });

        // TODO: test DELETE, POST, PUT routes
    });
    
    describe('orquestrator', () => {
        const path_local = '/localroute';
        const path_orchestrator_bad = '/register_bogus';
        const path_orchestrator_good = '/register_good';

        before(() => {
            // Setup routes for registration with orchestrator using different properties
            service.route(path_local, (request, response) => response.send('hello world'));
            service.route(path_orchestrator_bad, (request, response) => response.send('text data'));
            service.route(path_orchestrator_good, (request, response) => response.send({status: 'OK', data: 'Success'}));
        })

        it('failed registration because orchestrator does not exist', async () => {
            const orchestrator_url = url.parse('http://127.0.0.1:3000');
            let errored = false;
            try {
                await service.register(orchestrator_url, 'service', path_local);
            } catch (err) {
                errored = true;
            } finally {
                assert.equal(errored, true);
            }
        });

        it('failed registration and no promise rejection handling', done => {
            const orchestrator_url = url.parse('http://127.0.0.1:3000');
            const res = service.register(orchestrator_url, 'service', path_local);
            setTimeout(done, 100); // do not exit immediately to allow error to be thrown
        });

        it('failed registration because orchestrator returns bogus data', async () => {
            const orchestrator_url = {
                hostname: '127.0.0.1', port: '' + service.server.address().port, pathname: path_orchestrator_bad};
            let errored = false;
            try {
                await service.register(orchestrator_url, 'service', path_local);
            } catch (err) {
                errored = true;
            } finally {
                assert.equal(errored, true);
            }
        });

        it('successful registration', async () => {
            const orchestrator_url = {
                hostname: '127.0.0.1', port: '' + service.server.address().port, pathname: path_orchestrator_good};
            await service.register(orchestrator_url, 'service', path_local);
        });

        it('successful registration with no path', async () => {
            // Setup dummy route that returns expected data
            const path = '/register';
            service.route(path, (request, response) => response.send({status: 'OK', data: 'Success'}));
            const orchestrator_url = {
                hostname: '127.0.0.1', port: '' + service.server.address().port};
            await service.register(orchestrator_url, 'service', path_local);
        });
    });

    describe('benchmark', () => {
        let tmplog: (level: string, ...args: any[]) => Promise<void> = null;
        beforeEach(async () => {
            tmplog = service.log;
            service.log = async (level: string, ...args: any[]) => {};
        });
        afterEach(() => {
            service.log = tmplog;
        });

        it('cached', async () => {
            const start = new Date().getTime();
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await request(service.server).get('/1');
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('dummy', async () => {
            const start = new Date().getTime();
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await request(service.server).get('/random?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('root (invalid)', async () => {
            const start = new Date().getTime();
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await request(service.server).get('/?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);
    });

});
