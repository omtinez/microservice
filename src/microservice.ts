import * as bodyParser from 'body-parser';
import * as child_process from 'child_process';
import * as express from 'express';
import * as http from 'http';
import * as moment from 'moment';
import * as path from 'path';
import * as url from 'url';
import * as sqlite from 'sqlite';
import { ProcShell, Requests } from 'ootils';

export interface ServiceRoute {
    local: string; // path in service
    remote: string; // path in orchestrator
}
export interface RouteOptions {
    method: string,
    globalCache?: boolean,
    optionalQueryParameters?: string[];
    mandatoryQueryParameters?: string[];
}
export type RouteCallback = (request: express.Request, response: express.Response) => void;

/** Represents a self-contained service with its own routes, cache and backing storage */
export class MicroService {

    // Class fields
    /** Pointer to the backing storage used by this service */
    public db: sqlite.Database;
    /** Pointer to the underlying Node HTTP server powering this service */
    public server: http.Server;
    /** Pointer to the underlying Express application powering this service */
    public routes: express.Express;
    public routeOptions: {[path: string]: RouteOptions} = {};
    public cacheExpirySeconds: number;

    // Defeat instantiation
    private constructor() {}

    static create(dbfilename?: string, cacheExpirySeconds = 60): Promise<MicroService> {
        return new Promise<MicroService>(async (resolve, reject) => {
            const service = new MicroService();

            // Database setup
            service.cacheExpirySeconds = cacheExpirySeconds;
            service.db = await sqlite.open(dbfilename || ':memory:');
            await service.db.run(`
                CREATE TABLE IF NOT EXISTS log (
                    level TEXT NOT NULL,
                    message TEXT NOT NULL,
                    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)`);
            await service.db.run(`PRAGMA journal_mode = WAL;`);
            await service.db.run(`PRAGMA busy_timeout = 15000;`);

            // Initialize cache
            await service.db.run(`
                CREATE TABLE IF NOT EXISTS cache (
                    url           TEXT NOT NULL,
                    headers_in    TEXT NOT NULL,
                    headers_out   TEXT NOT NULL,
                    body          TEXT,
                    status        INT,
                    timestamp     DATETIME DEFAULT CURRENT_TIMESTAMP)`)
            await service.db.run(`
                DELETE FROM cache
                    WHERE timestamp < datetime('now', '-${service.cacheExpirySeconds} seconds')`);

            // Middleware setup
            service.routes = express();
            service.routes.use(express.static(path.join(__dirname, 'static')));
            service.routes.use(bodyParser.json());

            // Resolve with service now that database has been initialized
            resolve(service);
        });
    }

    /** Logs an event in the console as well as the internal database. Level can be E, W, I and V */
    async log(level: string, ...args: any[]) {
        if (level.length > 1) {
            args = [level].concat(args);
            level = 'I';
        }
        console.log.apply(this, [moment().format(), level].concat(args));
        try {
            await this.db.run(`INSERT INTO log (level, message) VALUES (?, ?)`, [level, args.join('\t')]);
        } catch (err) {
            this.log('E', 'Error updating cache: ' + err.message);
        }
    }

    async clearCache(all = false) {
        try {
            await this.db.run(`DELETE FROM cache  
                ${all ? "" : `WHERE timestamp < datetime('now', '-${this.cacheExpirySeconds} seconds')`}`);
        } catch (err) {
            this.log('E', err.message);
        }
    }

    queryCache(url: string, headers: string): Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            try {
                const row = await this.db.get(`
                    SELECT * FROM cache WHERE url=(?) AND headers_in=(?)
                    AND timestamp > datetime('now', '-${this.cacheExpirySeconds} seconds') LIMIT 1`,
                    [url, headers]);
                if (row && row.status !== 503) {
                    this.log('V', `[${row.status}] Serving result from cache`);
                    resolve(row);
                } else {
                    resolve();
                }
            } catch (err) {
                this.log('E', err.message);
                resolve();
            }
        })
    }

    async updateCache(url: string, headers_in: string, headers_out: string, body: string, status: number) {
        try {
            await this.db.run(`
                INSERT INTO cache (url, headers_in, headers_out, body, status)
                VALUES (?, ?, ?, ?, ?)`, [url, headers_in, headers_out, body, status]);
        } catch (err) {
            this.log('E', 'Error occurred updating cache: ' + err.message);
        }
    }

    /**
     * Sets up a route for the microservice to handle
     * @param path the location of the route, e.g. "/endpoint"
     * @param callback the callback function that will be performed by this route for every request
     * @param options either a `RouteOptions` or a string with the method that this route accepts
     */
    route(path: string, callback: RouteCallback, options: RouteOptions | string = 'GET'): Promise<void> {
        return new Promise<void>((resolve, reject) => {

            // Parse route options
            let opts: RouteOptions;
            if (typeof options === 'string') {
                opts = {method: options};
            } else {
                opts = options;
            }

            // Check that route has not been already registered
            if (this.routeOptions[path]) {
                const msg = `Route ${path} already exists`;
                reject(new Error(msg));
                this.log('E', msg);

            } else {
                // Otherwise, record route with options and setup with express
                this.routeOptions[path] = opts;

                // Use reflection to setup the route with the server
                this.log('V', `Setting up ${opts.method} route for ${path}`);
                const routeFunction: Function = (this.routes as any)[opts.method.toLowerCase()];
                routeFunction.call(this.routes, path, async (request: express.Request, response: express.Response) => {

                    // Log event
                    this.log('V', opts.method, path, request.url);

                    // Return from cache if available
                    //console.log(request.rawHeaders)
                    const headers_in = '' + (opts.globalCache || JSON.stringify(request.rawHeaders));
                    const data = await this.queryCache(request.url, headers_in);
                    if (data) {
                        response.status(data.status).header(JSON.parse(data.headers_out)).end(data.body);

                    // No result in the cache, query online
                    } else {
                        // Monkey-patch the response.send function to enable caching
                        const _send = response.send;
                        response.send = (body?: any): express.Response => {
                            if (typeof body === 'object') {
                                response.contentType('application/json');
                                body = JSON.stringify(body);
                            }
                            const headers_out = JSON.stringify((response as any).header()._headers);
                            this.updateCache(request.url, headers_in, headers_out, body, response.statusCode);
                            _send.call(response, body);
                            return response;
                        }

                        // Verify parameters if any
                        if (opts.mandatoryQueryParameters &&
                            !opts.mandatoryQueryParameters.every(param => request.query.hasOwnProperty(param))) {
                                const paramcsv = 
                                    opts.mandatoryQueryParameters.map(param => `"${param}"`).join(', ');
                                const msg = `Parameters ${paramcsv} are mandatory`;
                                response.status(400).send({status: 'ERR', data: msg});

                        } else {
                            // Perform actual callback
                            callback(request, response);
                        }
                    }
                });

                // Finally, resolve promise
                resolve();
            }
        });
    }

    /**
     * Signal other MicroService-aware components, such as SystemD services and
     * microservice-spawner that the service is ready.
     */
    notify(): void {
        // Signal parent process that server has started by disconnecting from the IPC channel
        if (process.connected) {
            process.disconnect();
        }

        // Signal systemd service (ignore if systemd-notify does not exist)
        ProcShell.which('systemd-notify').then(bin => {
            ProcShell.exec(`${bin} --ready`).catch(err => {
                this.log('E', 'Unable to notify systemd daemon: ' + err.message);
            });
        }).catch(err => {});
    }

    /** Register a path from this service with a microservice-orchestrator service */
    async register(orchestrator: url.Url, name: string, path: ServiceRoute | string, notify = true) {
        const route = typeof path === 'string' ? {local: path, remote: path} : path;

        const pathname = orchestrator.pathname && orchestrator.pathname.length > 1 ?
            orchestrator.pathname : 'register'
        const formatted = url.format(<url.Url> {
            pathname: pathname,
            port: orchestrator.port,
            protocol: orchestrator.protocol || 'http',
            hostname: orchestrator.hostname || '127.0.0.1',
            query: {
                name: name,
                local_path: typeof path === 'string' ? path : path.local,
                remote_path: typeof path === 'string' ? path : path.remote,
                spec: JSON.stringify(this.routeOptions[route.local]),
                port: this.server.address().port
            },
        });
        const res = JSON.parse(await Requests.get(formatted));
        const msg = res.data;
        this.log('V', msg);
        if (notify) {
            this.notify();
        }
    }

    start(port: number) {
        this.log('I', 'Starting microservice at port ' + port);
        this.server = this.routes.listen(port);
    }

    stop() {
        if (this.server && this.server.listening) {
            this.server.close();
        }
    }
}
