var gulp = require('gulp');
var ts = require('gulp-typescript');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');


// Clean tasks

gulp.task('clean', function() {
    return gulp.src('dist', { read: false })
    	.pipe(clean());
});


// Build tasks

gulp.task('ts', function() {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            target: 'ES2015',
            module: 'commonjs',
            declaration: true,
            noImplicitAny: true,
         }))
        .pipe(gulp.dest('dist'));
});

gulp.task('build', gulp.series('ts'));
gulp.task('default', gulp.series('build'));
