/// <reference types="express" />
/// <reference types="node" />
import * as express from 'express';
import * as http from 'http';
import * as url from 'url';
import * as sqlite from 'sqlite';
export interface ServiceRoute {
    local: string;
    remote: string;
}
export interface RouteOptions {
    method: string;
    globalCache?: boolean;
    optionalQueryParameters?: string[];
    mandatoryQueryParameters?: string[];
}
export declare type RouteCallback = (request: express.Request, response: express.Response) => void;
/** Represents a self-contained service with its own routes, cache and backing storage */
export declare class MicroService {
    /** Pointer to the backing storage used by this service */
    db: sqlite.Database;
    /** Pointer to the underlying Node HTTP server powering this service */
    server: http.Server;
    /** Pointer to the underlying Express application powering this service */
    routes: express.Express;
    routeOptions: {
        [path: string]: RouteOptions;
    };
    cacheExpirySeconds: number;
    private constructor();
    static create(dbfilename?: string, cacheExpirySeconds?: number): Promise<MicroService>;
    /** Logs an event in the console as well as the internal database. Level can be E, W, I and V */
    log(level: string, ...args: any[]): Promise<void>;
    clearCache(all?: boolean): Promise<void>;
    queryCache(url: string, headers: string): Promise<any>;
    updateCache(url: string, headers_in: string, headers_out: string, body: string, status: number): Promise<void>;
    /**
     * Sets up a route for the microservice to handle
     * @param path the location of the route, e.g. "/endpoint"
     * @param callback the callback function that will be performed by this route for every request
     * @param options either a `RouteOptions` or a string with the method that this route accepts
     */
    route(path: string, callback: RouteCallback, options?: RouteOptions | string): Promise<void>;
    /**
     * Signal other MicroService-aware components, such as SystemD services and
     * microservice-spawner that the service is ready.
     */
    notify(): void;
    /** Register a path from this service with a microservice-orchestrator service */
    register(orchestrator: url.Url, name: string, path: ServiceRoute | string, notify?: boolean): Promise<void>;
    start(port: number): void;
    stop(): void;
}
