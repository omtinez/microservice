"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const express = require("express");
const moment = require("moment");
const path = require("path");
const url = require("url");
const sqlite = require("sqlite");
const ootils_1 = require("ootils");
/** Represents a self-contained service with its own routes, cache and backing storage */
class MicroService {
    // Defeat instantiation
    constructor() {
        this.routeOptions = {};
    }
    static create(dbfilename, cacheExpirySeconds = 60) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const service = new MicroService();
            // Database setup
            service.cacheExpirySeconds = cacheExpirySeconds;
            service.db = yield sqlite.open(dbfilename || ':memory:');
            yield service.db.run(`
                CREATE TABLE IF NOT EXISTS log (
                    level TEXT NOT NULL,
                    message TEXT NOT NULL,
                    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)`);
            yield service.db.run(`PRAGMA journal_mode = WAL;`);
            yield service.db.run(`PRAGMA busy_timeout = 15000;`);
            // Initialize cache
            yield service.db.run(`
                CREATE TABLE IF NOT EXISTS cache (
                    url           TEXT NOT NULL,
                    headers_in    TEXT NOT NULL,
                    headers_out   TEXT NOT NULL,
                    body          TEXT,
                    status        INT,
                    timestamp     DATETIME DEFAULT CURRENT_TIMESTAMP)`);
            yield service.db.run(`
                DELETE FROM cache
                    WHERE timestamp < datetime('now', '-${service.cacheExpirySeconds} seconds')`);
            // Middleware setup
            service.routes = express();
            service.routes.use(express.static(path.join(__dirname, 'static')));
            service.routes.use(bodyParser.json());
            // Resolve with service now that database has been initialized
            resolve(service);
        }));
    }
    /** Logs an event in the console as well as the internal database. Level can be E, W, I and V */
    log(level, ...args) {
        return __awaiter(this, void 0, void 0, function* () {
            if (level.length > 1) {
                args = [level].concat(args);
                level = 'I';
            }
            console.log.apply(this, [moment().format(), level].concat(args));
            try {
                yield this.db.run(`INSERT INTO log (level, message) VALUES (?, ?)`, [level, args.join('\t')]);
            }
            catch (err) {
                this.log('E', 'Error updating cache: ' + err.message);
            }
        });
    }
    clearCache(all = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.db.run(`DELETE FROM cache  
                ${all ? "" : `WHERE timestamp < datetime('now', '-${this.cacheExpirySeconds} seconds')`}`);
            }
            catch (err) {
                this.log('E', err.message);
            }
        });
    }
    queryCache(url, headers) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const row = yield this.db.get(`
                    SELECT * FROM cache WHERE url=(?) AND headers_in=(?)
                    AND timestamp > datetime('now', '-${this.cacheExpirySeconds} seconds') LIMIT 1`, [url, headers]);
                if (row && row.status !== 503) {
                    this.log('V', `[${row.status}] Serving result from cache`);
                    resolve(row);
                }
                else {
                    resolve();
                }
            }
            catch (err) {
                this.log('E', err.message);
                resolve();
            }
        }));
    }
    updateCache(url, headers_in, headers_out, body, status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.db.run(`
                INSERT INTO cache (url, headers_in, headers_out, body, status)
                VALUES (?, ?, ?, ?, ?)`, [url, headers_in, headers_out, body, status]);
            }
            catch (err) {
                this.log('E', 'Error occurred updating cache: ' + err.message);
            }
        });
    }
    /**
     * Sets up a route for the microservice to handle
     * @param path the location of the route, e.g. "/endpoint"
     * @param callback the callback function that will be performed by this route for every request
     * @param options either a `RouteOptions` or a string with the method that this route accepts
     */
    route(path, callback, options = 'GET') {
        return new Promise((resolve, reject) => {
            // Parse route options
            let opts;
            if (typeof options === 'string') {
                opts = { method: options };
            }
            else {
                opts = options;
            }
            // Check that route has not been already registered
            if (this.routeOptions[path]) {
                const msg = `Route ${path} already exists`;
                reject(new Error(msg));
                this.log('E', msg);
            }
            else {
                // Otherwise, record route with options and setup with express
                this.routeOptions[path] = opts;
                // Use reflection to setup the route with the server
                this.log('V', `Setting up ${opts.method} route for ${path}`);
                const routeFunction = this.routes[opts.method.toLowerCase()];
                routeFunction.call(this.routes, path, (request, response) => __awaiter(this, void 0, void 0, function* () {
                    // Log event
                    this.log('V', opts.method, path, request.url);
                    // Return from cache if available
                    //console.log(request.rawHeaders)
                    const headers_in = '' + (opts.globalCache || JSON.stringify(request.rawHeaders));
                    const data = yield this.queryCache(request.url, headers_in);
                    if (data) {
                        response.status(data.status).header(JSON.parse(data.headers_out)).end(data.body);
                        // No result in the cache, query online
                    }
                    else {
                        // Monkey-patch the response.send function to enable caching
                        const _send = response.send;
                        response.send = (body) => {
                            if (typeof body === 'object') {
                                response.contentType('application/json');
                                body = JSON.stringify(body);
                            }
                            const headers_out = JSON.stringify(response.header()._headers);
                            this.updateCache(request.url, headers_in, headers_out, body, response.statusCode);
                            _send.call(response, body);
                            return response;
                        };
                        // Verify parameters if any
                        if (opts.mandatoryQueryParameters &&
                            !opts.mandatoryQueryParameters.every(param => request.query.hasOwnProperty(param))) {
                            const paramcsv = opts.mandatoryQueryParameters.map(param => `"${param}"`).join(', ');
                            const msg = `Parameters ${paramcsv} are mandatory`;
                            response.status(400).send({ status: 'ERR', data: msg });
                        }
                        else {
                            // Perform actual callback
                            callback(request, response);
                        }
                    }
                }));
                // Finally, resolve promise
                resolve();
            }
        });
    }
    /**
     * Signal other MicroService-aware components, such as SystemD services and
     * microservice-spawner that the service is ready.
     */
    notify() {
        // Signal parent process that server has started by disconnecting from the IPC channel
        if (process.connected) {
            process.disconnect();
        }
        // Signal systemd service (ignore if systemd-notify does not exist)
        ootils_1.ProcShell.which('systemd-notify').then(bin => {
            ootils_1.ProcShell.exec(`${bin} --ready`).catch(err => {
                this.log('E', 'Unable to notify systemd daemon: ' + err.message);
            });
        }).catch(err => { });
    }
    /** Register a path from this service with a microservice-orchestrator service */
    register(orchestrator, name, path, notify = true) {
        return __awaiter(this, void 0, void 0, function* () {
            const route = typeof path === 'string' ? { local: path, remote: path } : path;
            const pathname = orchestrator.pathname && orchestrator.pathname.length > 1 ?
                orchestrator.pathname : 'register';
            const formatted = url.format({
                pathname: pathname,
                port: orchestrator.port,
                protocol: orchestrator.protocol || 'http',
                hostname: orchestrator.hostname || '127.0.0.1',
                query: {
                    name: name,
                    local_path: typeof path === 'string' ? path : path.local,
                    remote_path: typeof path === 'string' ? path : path.remote,
                    spec: JSON.stringify(this.routeOptions[route.local]),
                    port: this.server.address().port
                },
            });
            const res = JSON.parse(yield ootils_1.Requests.get(formatted));
            const msg = res.data;
            this.log('V', msg);
            if (notify) {
                this.notify();
            }
        });
    }
    start(port) {
        this.log('I', 'Starting microservice at port ' + port);
        this.server = this.routes.listen(port);
    }
    stop() {
        if (this.server && this.server.listening) {
            this.server.close();
        }
    }
}
exports.MicroService = MicroService;
